package com.test.school;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.google.gson.Gson;

@Path("/School")
public class SchoolService {
	@GET
	@Path("/getFeeds")
	@Produces("apllication/json")
	public String feed(){
		System.out.println("inside feed() of school service");
		String feeds = null;
		try{
			ArrayList<SchoolObjects> feedData=null;
			Model model=new Model();
			feedData=model.getFeeds();
			Gson gson=new Gson();
			System.out.println(gson.toJson(feedData));
			feeds=gson.toJson(feedData);
			
			
		}catch(Exception e){
			System.out.println("Exception Error");
			
		}
		return feeds;
	}
	

}
