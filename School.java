package com.test.school;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class School {

	
	public ArrayList< SchoolObjects> GetSchoolObjects(Connection connection) throws Exception{
	
		ArrayList<SchoolObjects> schoolData=new ArrayList<SchoolObjects>();
		try{
			System.out.println("To get details of schools");
			PreparedStatement ps=connection.prepareStatement("Select name,address,pincode from SCHOOL order by id desc");
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				SchoolObjects schoolObject= new SchoolObjects();
				schoolObject.setName(rs.getString("name"));
				schoolObject.setAddress(rs.getString("address"));
				schoolObject.setPincode(rs.getString("pincode"));
				schoolData.add(schoolObject);
				
			}
			return schoolData;
			
		}
		catch(Exception e){
			throw e;
		}
	
		
	}

}
