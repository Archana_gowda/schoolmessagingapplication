package com.test.school;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;


public class Model {
	
	public ArrayList<SchoolObjects> getFeeds() throws Exception{
		
		ArrayList< SchoolObjects> feeds=null;
		Connection connection=null;
		try {
			System.out.println("To get connection of database");
			Class.forName(Constant.DRIVER);
			connection=DriverManager.getConnection(Constant.URL, Constant.UID, Constant.PASS);
			if(connection!=null){
				System.out.println("Connection established successfully....");
			School school=new School();
			feeds=school.GetSchoolObjects(connection);
			}
		}catch(Exception e){
		     System.out.println("Connection failed...");
		     e.printStackTrace();
		}
		return feeds;
		
	}

}
